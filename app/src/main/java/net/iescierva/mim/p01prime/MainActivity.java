package net.iescierva.mim.p01prime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static String TAG="p01prime:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Event handler
        Button b = findViewById(R.id.button);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG,"entrando en OnCreate");
        TextView numero=findViewById(R.id.editTextNumero);
        TextView resultado=findViewById(R.id.textViewResultado);
        Locale l=Locale.getDefault();

        String s=numero.getText().toString();
        int n=Integer.parseInt(s);

        PrimeNumbersCollection c;
        try {
            c = new PrimeNumbersCollection(n);
            resultado.setText(String.format(l,"%d",c.get(n) ));
        } catch (Exception e) {
            resultado.setText(getResources().getText(R.string.excepcion));
        }  // finally {}

        Log.d(TAG,"saliendo de OnCreate");
    }
}
